import json
from httplib2 import Http
from urllib.parse import urlencode

ap_id = "57d298d118a95a927e02238438767913"
nick = "Baumfaust"
#nick = "1fortheMoney2fortheShow"

def loadAccount(nick):
    h = Http(".cache")
    data = {"application_id": ap_id, "search": nick }
    resp, content = h.request("https://api.worldoftanks.eu/wot/account/list/", "POST", urlencode(data))
    player = json.loads(content.decode("utf-8"))
    return str(player["data"][0]["account_id"])

def loadTanksInGarageS(account_id):
    h = Http(".cache")
    data = {"application_id": ap_id, "account_id": account_id, "in_garage": "1", "fields": "tank_id" }
    resp, content = h.request("https://api.worldoftanks.eu/wot/tanks/stats/", "POST", urlencode(data))
    return json.loads(content.decode("utf-8"))

def loadTank_deprecaded(tank_id):
    h = Http(".cache")
    data = {"application_id": ap_id, "tank_id": tank_id, "fields": "name_i18n,nation_i18n" }
    resp, content = h.request("https://api.worldoftanks.eu/wot/encyclopedia/tankinfo/", "POST", urlencode(data))
    return json.loads(content.decode("utf-8"))

def loadTank(tank_id):
    h = Http(".cache")
    data = {"application_id": ap_id, "tank_id": tank_id, "fields": "name,nation" }
    resp, content = h.request("https://api.worldoftanks.eu/wot/encyclopedia/vehicles/", "POST", urlencode(data))
    return json.loads(content.decode("utf-8"))

def loadTanksInGarage(account_id):
    h = Http(".cache")
    data = {"application_id": ap_id, "account_id": account_id, "in_garage": "1", "fields": "tank_id,max_frags,all" }
    resp, content = h.request("https://api.worldoftanks.eu/wot/tanks/stats/", "POST", urlencode(data))
    return json.loads(content.decode("utf-8"))

account_id = loadAccount(nick)
tanks = loadTanksInGarage(account_id)
print(account_id)


totalBattles = 0
print("Tanks of N00B: "+nick)
for tank in tanks["data"][account_id]:
    # get tank details
    tank_id = str(tank["tank_id"])
    tankdetails = loadTank(tank["tank_id"])
    totalBattles= totalBattles + tank["all"]["battles"]
    if ( tank["all"]["battles"] >= 50 ):
        battles = tank["all"]["battles"]
        wins = tank["all"]["wins"]
        rate = wins * 100 / battles
        print(tankdetails["data"][tank_id]["name"]+ ", "+tankdetails["data"][tank_id]["nation"])
        print("Battles:  {0:5d}".format(battles))
        print("Wins:     {0:5d}".format(wins))
        print("Losses:   {0:5d}".format(tank["all"]["losses"]))
        print("Rate:        {0:3.1f}%".format(rate))
        print("max. Frags:  {0:2d}".format(tank["max_frags"]))
        print()

print("Battles total: " + str(totalBattles))

